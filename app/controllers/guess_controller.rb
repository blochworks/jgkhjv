
class GuessController < ApplicationController

	def randomWord()
		path = ("#{Rails.root}/app/assets/danish_words.json")
		file = open(path, mode: "r:windows-1252:utf-8").read
		data = JSON.parse(file)
		return data[rand(0..data.length-1)].capitalize
	end

	def answerTo(word)

		winner = ["Arrh... æv - så vandt du!!!", "Jeps! Så vinder du!", "Tillykke!!! Lige i øjet, godt gået", "Aaarrgh... jeg var så tæt på - tillykke!", "Well done kamerat", "Helt sikkert mester!!!"]
		close = ["phew... close but no cigar", "Årrh... hvad det nærmer sig...", "uuuhha.... ", "Årh det er solidt spillet"]
		role = ["ligger godt i mave"]
		
		if(word.downcase == "daloon") 
			return {:word => word, :extra => winner[rand(0..winner.length-1)], :cpuGuess => "", :animate => false, :end => true}
		end
		if(word.downcase == "sofahynde") 
			return {:word => word, :extra => close[rand(0..close.length-1)], :cpuGuess => randomWord(), :animate => false, :end => false}
		end
		if(word.downcase == "forårsrulle") 
			return {:word => word, :extra => role[rand(0..role.length-1)], :cpuGuess => randomWord(), :animate => false, :end => false}
		end
		if(word.downcase == "bambus") 
			return {:word => word, :extra => "Vokser halv... eller en hele meter... om dagen", :cpuGuess => randomWord(), :animate => false, :end => false}
		end
		return {:word => word, :cpuGuess => randomWord(), :animate => false, :end => false}
	end

	def playerGuess
		word = params[:word]
		cpuGuess = answerTo(word)
		render json: cpuGuess
	end

end
